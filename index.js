const log4js = require('log4js');

const logger = log4js.getLogger();
logger.level = "error";

logger.debug("Iniciando aplicacion en modo de pruebas.");
logger.info("La app ha iniciado correctamente");
logger.warn("Falta el archivo config de la app");
logger.error("No se pudo acceder al sistema de archivos");
logger.fatal("Aplicacion no se pudo ejecutar en el so");

let x_1 = 0;

function sumar(x,y){
    return x + y;
}

module.exports = sumar; // Para exportar funciones