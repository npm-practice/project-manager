const sumar = require('../index');
const assert = require('assert');


describe("Probar la suma de dos numeros", ()=>{
    //Afirmar que 5 + 5 son 10
    it("Cinco mas Cinco es 10", ()=>{
        assert.equal(10, sumar(5,5));
    });

    //Afirmamos que cinco mas cinco no son 55
    it("Cinco mas cinco no son 55", ()=>{
        assert.notEqual(55, sumar(5,5));
    });

});